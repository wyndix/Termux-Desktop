#!/usr/bin/bash

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

bash -c "$SCRIPT_DIR/variants/KDE/main.sh"

echo "Installation is done"