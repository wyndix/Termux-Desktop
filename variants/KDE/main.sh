#!/usr/bin/bash

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

main() {
    # upgrades the system before continuing
    apt update -y && apt full-upgrade -y

    # removes unneccesary dependencies to save up space (we'll need a ton...)
    apt autoremove -y
    mv $HOME/.config/TDE/source/variants/KDE/distro_plugins $PREFIX/etc/proot-distro
    
    proot-distro install tersolinux
    proot-distro login tersolinux
}

main