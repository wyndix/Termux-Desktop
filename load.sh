#!/usr/bin/bash

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

GIT_REPO="https://codeberg.org/wyndix/Termux-Desktop.git"

# Check if Git is installed, if not, install it with apt
checkinstall() {
    echo "Checking if $1 is installed..."

    if ! [ -x "$(command -v $1)" ]; then
        if ! [ -x "$(command -v apt)" ]; then
            echo "This script only works on apt-based systems"
            exit
        fi
        echo "$1 is not installed, installing..."
        apt update -y
        apt install $1 -y
    fi
    echo "$1 is installed."
}

main() {
    checkinstall dialog

    if ! (command -v termux-setup-storage); then
        if ! (dialog --title "Non-Termux warning" --backtitle "TDE installer" --yesno "You seem to not be running this script on Termux, if this is the case please close it immediately. This program can cause tons of issues in a non-Termux distribution. Do you want to proceed?" 9 50); then
            exit 1
        fi
    fi

    if ! (dialog --title "Warning" --backtitle "TDE installer" --yesno "This installer will perform a bunch of destructive actions to your Termux install, if you don't want this, please use the Shelter app to install another instance of it. Do you want to proceed?" 9 50); then
        exit 1
    fi

    # clear

    checkinstall git

    # Write the git repo into ~/.config/TDE/GitRemote
    mkdir -p  "$HOME/.config/TDE"
    mkdir  "$HOME/.config/TDE/source"

    git clone "$GIT_REPO" "$HOME/.config/TDE/source"

    echo "$GIT_REPO" > "$HOME/.config/TDE/.gitRemote"

    bash "$HOME/.config/TDE/source/install.sh"

    echo "My job as a loader is done"
}

main